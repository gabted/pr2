#warnings "-8";;


type ide = string;;
type exp =
	Int of int
	|Bool of bool
	|And of exp*exp
	|Or of exp*exp
	|Not of exp
	|Sum of exp*exp
	|Difference of exp*exp
	|Product of exp*exp
	|Division of exp*exp
	|Equals of exp*exp
	|LessThen of exp*exp
	|IfElseExp of exp*exp*exp
	|FunCallExp of ide*exp;;

type declaration =
	FunDeclaration of ide*ide*exp;;

type expValue =
	IntValue of int
	|BoolValue of bool;;

type envT = Unbound | FunVal of ide*exp;;
type enviroment = ide -> envT;;
let emptyEnv = function x -> Unbound;;

let evalFunction (f : ide) (e : exp) : expValue = 
	IntValue(0);;

let rec evalExp (e : exp) (env : enviroment) : expValue= match e with
	Int n -> IntValue n
	|Bool b -> BoolValue b
	|And(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(BoolValue(x), BoolValue(y)) -> BoolValue (x&&y))
	|Or(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(BoolValue(x), BoolValue(y)) -> BoolValue (x||y))
	|Not a -> let aValue = evalExp a env in
				  (match aValue with
					BoolValue(x) -> match x with true -> BoolValue(false) 
												|false -> BoolValue(true))

	|Sum(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> IntValue (x+y))
	|Difference(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> IntValue (x-y))
	|Product(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> IntValue (x*y))
	|Division(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> IntValue (x/y))
	|Equals(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> BoolValue (x=y))
	|LessThen(a, b) -> let aValue = evalExp a env in
				  let bValue = evalExp b env in
				  (match (aValue, bValue) with
					(IntValue(x), IntValue(y)) -> BoolValue (x<=y))
	|IfElseExp(test, thenBranch, elseBranch) -> let testValue = evalExp test env in
												(match testValue with
												BoolValue(result)->if (result)
																		then evalExp thenBranch env
																	else
																		evalExp elseBranch env)
	|FunCallExp(f, x) -> evalFunction f x;;
	
	


let print (x : expValue) = match x with
		IntValue(out) -> print_int(out);print_string("\n")
		|BoolValue(out) ->if out then print_string("true\n")
						else print_string("false\n");;



let exp1 = Sum(Int(40), Int(2));;
print (evalExp exp1 emptyEnv);;

let exp2 = And(Bool(true), Bool(false));;
print (evalExp exp2 emptyEnv);;
let exp3 = Or(Bool(true), Bool(false));;
print (evalExp exp3 emptyEnv);;


let exp4 = IfElseExp(LessThen(exp1, Int(50)), Product(Int(7), Int(7)), Int(0));;
print (evalExp exp4 emptyEnv);;