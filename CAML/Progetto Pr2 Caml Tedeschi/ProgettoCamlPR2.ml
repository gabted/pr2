#warnings "-8";;

type ide = string;;
type exp = Eint of int | Ebool of bool | Den of ide | Prod of exp * exp | Sum of exp * exp | Diff of exp * exp |
	Eq of exp * exp | Minus of exp | IsZero of exp | Or of exp * exp | And of exp * exp | Not of exp |
	Ifthenelse of exp * exp * exp | Let of ide * exp * exp | Fun of ide * exp | FunCall of exp * exp |
	Letrec of ide * exp * exp
	(*clear è equivalente a myDict = {}*)
	|DictInit of (ide*exp) list(*da usare in let myDict = {a:A, b:B, ...} in ...*)
	|DictUse of exp*ide (*sintassi concreta exp[ide], che è equivalente a exp.get(ide)*)
	|DictModify of exp*ide*exp(*sintassi concreta exp[ide]=exp, che aggiorna un valore o ne aggiunge uno nuovo*)
	|DictRemove of exp*ide(*sin. conc. rm(exp, ide)*)
	|DictForEach of exp*exp(*sin. conc. applyOver(exp, exp), dove la prima exp è una funzione*)
	;;

(*ambiente polimorfo*)
type 't env = ide -> 't;;
let emptyenv (v : 't) = function x -> v;;
let applyenv (r : 't env) (i : ide) = r i;;
let bind (r : 't env) (i : ide) (v : 't) = function x -> if x = i then v else applyenv r x;;

(*tipi esprimibili*)
type evT = Int of int | Bool of bool | Unbound | FunVal of evFun | RecFunVal of ide * evFun | Dictionary of (ide -> evT)
			| DynamicFunVal of ide * exp| DynamicRecFunVal of ide*ide*exp
and evFun = ide * exp * evT env

(*rts*)
(*type checking*)
let typecheck (s : string) (v : evT) : bool = match s with
	"int" -> (match v with
		Int(_) -> true |
		_ -> false) |
	"bool" -> (match v with
		Bool(_) -> true |
		_ -> false) |
	_ -> failwith("not a valid type");;

(*funzioni primitive*)
let prod x y = if (typecheck "int" x) && (typecheck "int" y)
	then (match (x,y) with
		(Int(n),Int(u)) -> Int(n*u))
	else failwith("Type error");;

let sum x y = if (typecheck "int" x) && (typecheck "int" y)
	then (match (x,y) with
		(Int(n),Int(u)) -> Int(n+u))
	else failwith("Type error");;

let diff x y = if (typecheck "int" x) && (typecheck "int" y)
	then (match (x,y) with
		(Int(n),Int(u)) -> Int(n-u))
	else failwith("Type error");;

let eq x y = if (typecheck "int" x) && (typecheck "int" y)
	then (match (x,y) with
		(Int(n),Int(u)) -> Bool(n=u))
	else failwith("Type error");;

let minus x = if (typecheck "int" x) 
	then (match x with
	   	Int(n) -> Int(-n))
	else failwith("Type error");;

let iszero x = if (typecheck "int" x)
	then (match x with
		Int(n) -> Bool(n=0))
	else failwith("Type error");;

let vel x y = if (typecheck "bool" x) && (typecheck "bool" y)
	then (match (x,y) with
		(Bool(b),Bool(e)) -> (Bool(b||e)))
	else failwith("Type error");;

let et x y = if (typecheck "bool" x) && (typecheck "bool" y)
	then (match (x,y) with
		(Bool(b),Bool(e)) -> Bool(b&&e))
	else failwith("Type error");;

let non x = if (typecheck "bool" x)
	then (match x with
		Bool(true) -> Bool(false) |
		Bool(false) -> Bool(true))
	else failwith("Type error");;

(*interprete*)
let rec eval (e : exp) (r : evT env) : evT = match e with
	Eint n -> Int n |
	Ebool b -> Bool b |
	IsZero a -> iszero (eval a r) |
	Den i -> applyenv r i |
	Eq(a, b) -> eq (eval a r) (eval b r) |
	Prod(a, b) -> prod (eval a r) (eval b r) |
	Sum(a, b) -> sum (eval a r) (eval b r) |
	Diff(a, b) -> diff (eval a r) (eval b r) |
	Minus a -> minus (eval a r) |
	And(a, b) -> et (eval a r) (eval b r) |
	Or(a, b) -> vel (eval a r) (eval b r) |
	Not a -> non (eval a r) |
	Ifthenelse(a, b, c) -> 
		let g = (eval a r) in
			if (typecheck "bool" g) 
				then (if g = Bool(true) then (eval b r) else (eval c r))
				else failwith ("nonboolean guard") |
	Let(i, e1, e2) -> eval e2 (bind r i (eval e1 r)) |
	Fun(i, a) -> FunVal(i, a, r) |
	FunCall(f, eArg) -> 
		let fClosure = (eval f r) in
			(match fClosure with
				FunVal(arg, fBody, fDecEnv) -> 
					eval fBody (bind fDecEnv arg (eval eArg r)) |
				RecFunVal(g, (arg, fBody, fDecEnv)) -> 
					let aVal = (eval eArg r) in
						let rEnv = (bind fDecEnv g fClosure) in
							let aEnv = (bind rEnv arg aVal) in
								eval fBody aEnv |
				_ -> failwith("non functional value"))|
    Letrec(f, funDef, letBody) ->
        		(match funDef with
            		Fun(i, fBody) -> let r1 = (bind r f (RecFunVal(f, (i, fBody, r)))) in
                         			                eval letBody r1 |
            		_ -> failwith("non functional def"))
	|DictInit(list) -> Dictionary(evalDictInit list r)
	|DictUse(dict, chiave) -> let dictValue = eval dict r in
								(match dictValue with 
								Dictionary(f) -> f chiave
								|_ -> Unbound)
	|DictModify(dict, chiave, valore) -> let dictValue = eval dict r in
											(match dictValue with
											Dictionary(f) -> let fNew y = if y = chiave then (eval valore r)
																		else f y
															 in
															Dictionary(fNew)
											|_ -> Unbound)
	|DictRemove(dict, chiave) -> let dictValue = eval dict r in
											(match dictValue with
											Dictionary(f) -> let fNew y = if y = chiave then Unbound
																		else f y
															 in
															Dictionary(fNew)
											|_ -> Unbound)
	|DictForEach(f, dict) -> let fClosure = (eval f r) in
									(*controllo se a f corrisponde una funzione, ricorsiva o no, 
									ma bisognerebbe controllarne anche il tipo: tutti i valori del dizionario
									devono essere omogenei e la funzione deve accettare input di quel tipo*)
									(match fClosure with
										FunVal(arg, fBody, fDecEnv) -> 
											Dictionary(concatenate f dict r)
										|RecFunVal(g, (arg, fBody, fDecEnv)) ->
											Dictionary(concatenate f dict r)
										|_ -> failwith("non functional value"))
	
and evalDictInit (list : (ide*exp) list) (r : evT env) : (ide->evT) = 
	(match list with 
	[] -> fun y -> Unbound	
	|(k, v)::rest -> let vValue = eval v r in 
						fun y -> if y = k then vValue
											else (evalDictInit rest r) y)
and concatenate (f : exp) (d : exp) (r : evT env) : (ide->evT) = 
	fun y -> if(eval (DictUse(d, y)) r) = Unbound then Unbound
			 else(eval (FunCall(f, DictUse(d, y))) r)
	(*se consideriamo d come una funzione, stiamo chiamando eval f(d(y)) r*)
	;;

	
(*interprete dinamico*)



let rec dynamic_eval (e : exp) (r : evT env) : evT = match e with
	Eint n -> Int n |
	Ebool b -> Bool b |
	IsZero a -> iszero (dynamic_eval a r) |
	Den i -> applyenv r i |
	Eq(a, b) -> eq (dynamic_eval a r) (dynamic_eval b r) |
	Prod(a, b) -> prod (dynamic_eval a r) (dynamic_eval b r) |
	Sum(a, b) -> sum (dynamic_eval a r) (dynamic_eval b r) |
	Diff(a, b) -> diff (dynamic_eval a r) (dynamic_eval b r) |
	Minus a -> minus (dynamic_eval a r) |
	And(a, b) -> et (dynamic_eval a r) (dynamic_eval b r) |
	Not a -> non (dynamic_eval a r) |
	Ifthenelse(a, b, c) -> 
		let g = (dynamic_eval a r) in
			if (typecheck "bool" g) 
				then (if g = Bool(true) then (dynamic_eval b r) else (dynamic_eval c r))
				else failwith ("nonboolean guard") |
	Let(i, e1, e2) -> dynamic_eval e2 (bind r i (dynamic_eval e1 r)) |
	Fun(i, a) -> DynamicFunVal(i, a) |
	FunCall(f, eArg) -> 
		let fClosure = (dynamic_eval f r) in
			(match fClosure with
				DynamicFunVal(arg, fBody) -> 
					dynamic_eval fBody (bind r arg (dynamic_eval eArg r)) |
				DynamicRecFunVal(g, arg, fBody) -> 
					let aVal = (dynamic_eval eArg r) in
						let rEnv = (bind r g fClosure) in
							let aEnv = (bind rEnv arg aVal) in
								dynamic_eval fBody aEnv |
				_ -> failwith("non functional value"))|
    Letrec(f, funDef, letBody) ->
        		(match funDef with
            		Fun(i, fBody) -> let r1 = (bind r f (DynamicRecFunVal(f, i, fBody))) in
                         			                dynamic_eval letBody r1 |
            		_ -> failwith("non functional def"))
	|DictInit(list) -> Dictionary(evalDictInit list r)
	|DictUse(dict, chiave) -> let dictValue = dynamic_eval dict r in
								(match dictValue with 
								Dictionary(f) -> f chiave
								|_ -> Unbound)
	|DictModify(dict, chiave, valore) -> let dictValue = dynamic_eval dict r in
											(match dictValue with
											Dictionary(f) -> let fNew y = if y = chiave then (dynamic_eval valore r)
																		else f y
															 in
															Dictionary(fNew)
											|_ -> Unbound)
	|DictRemove(dict, chiave) -> let dictValue = dynamic_eval dict r in
											(match dictValue with
											Dictionary(f) -> let fNew y = if y = chiave then Unbound
																		else f y
															 in
															Dictionary(fNew)
											|_ -> Unbound)
	|DictForEach(funz, dict) -> let fClosure = (dynamic_eval funz r) in
									(match fClosure with
										DynamicFunVal(arg, fBody) -> 
											Dictionary(concatenate funz dict r)
										|DynamicRecFunVal(g, arg, fBody) ->
											Dictionary(concatenate funz dict r)
										|_ -> failwith("non functional value"))
	
and evalDictInit (list : (ide*exp) list) (r : evT env) : (ide->evT) = 
	(match list with 
	[] -> fun y -> Unbound	
	|(k, v)::rest -> let vValue = dynamic_eval v r in 
						fun y -> if y = k then vValue
											else (evalDictInit rest r) y)
and concatenate (f : exp) (d : exp) (r : evT env) : (ide->evT) = 
	fun y -> (dynamic_eval (FunCall(f, DictUse(d, y))) r)
	(*se consideriamo d come una funzione, stiamo chiamando eval f(d(y)) r*)
	;;


let rt_eval (e : exp) : evT = 
	let empty = emptyenv Unbound in
		dynamic_eval e empty;;

(* =============================  TESTS  ================= *)
	

let print (x : evT) = match x with
		Int(out) -> print_int(out);print_string("\n")
		| Bool(out) ->if out then print_string("true\n")
						else print_string("false\n")
		| Unbound ->print_string("Unbound\n");;

		
print_string ("*********TEST INTERPRETE BASE**********\n");;
(*(y->y+1) 3*) 
let env0 = emptyenv Unbound;;
let e1 = FunCall(Fun("y", Sum(Den "y", Eint 1)), Eint 3);;
print (eval e1 env0);;
(*mi aspetto 105 e 15*)


print_string ("*********TEST DIZIONARIO***************\n");;
(*TEST dichiarazione e utilizzo*)
	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in myDict[Alice]*)
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("myDict"), "Alice");;
	let firstLetExp = Let("myDict", DictInit([("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), usageExp);;
	print (eval firstLetExp env0);;

	
(*TEST aggiornamento e aggiunta*)
	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in 
			let newDict = myDict[Bob] --> 5 in 
				newDict[Bob]*)
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("newDict"), "Bob");;
	let secondLetExp = Let("newDict", DictModify(Den("myDict"), "Bob", Eint(5)), usageExp);;
	let firstLetExp = Let("myDict", DictInit([	("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), secondLetExp);;
	print (eval firstLetExp env0);;

	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in 
			let newDict = myDict[Dave] --> 10 in 
				newDict[Dave]*)
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("newDict"), "Dave");;
	let secondLetExp = Let("newDict", DictModify(Den("myDict"), "Dave", Eint(10)), usageExp);;
	let firstLetExp = Let("myDict", DictInit([	("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), secondLetExp);;
	print (eval firstLetExp env0);;
	
	
(*TEST rimozione*)
	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in 
			let newDict = rm(myDict, Charlie)in 
				newDict[Charlie]*)
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("newDict"), "Charlie	");;
	let secondLetExp = Let("newDict", DictRemove(Den("myDict"), "Charlie"), usageExp);;
	let firstLetExp = Let("myDict", DictInit([	("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), secondLetExp);;
	print (eval firstLetExp env0);;


(*TEST ForEach ricorsivo e non*)
	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in 
			let increase = fun y -> y+100 in
				let newDict = ApplyOver(increase, myDict) in
					newDict[Alice]*)
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("newDict"), "Alice");;
	let thirdLetExp = Let("newDict", DictForEach(Den("increase"), Den("myDict")), usageExp);;
	let secondLetExp = Let("increase", Fun("y", Sum(Den "y", Eint(100))),thirdLetExp)
	let firstLetExp = Let("myDict", DictInit([	("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), secondLetExp);;
	print (eval firstLetExp env0);;
	
	(*let myDict = {Alice: 7, Bob:3, Charlie: 42} in 
			let rec fac y = if y = 1 then 1
							else 	y*fib(y-1) in
				let newDict = ApplyOver(fac, myDict) in
					newDict[Alice]*)
	let facDefinition = Fun("y", Ifthenelse( Eq(Den("y"), Eint(1)), Eint(1), Prod(Den("y"), FunCall(Den("fac"), Diff(Den("y"), Eint(1))))))
	let env0 = emptyenv Unbound;;
	let usageExp = DictUse(Den("newDict"), "Alice");;
	let thirdLetExp = Let("newDict", DictForEach(Den("fac"), Den("myDict")), usageExp);;
	let secondLetExp = Letrec("fac", facDefinition, thirdLetExp)
	let firstLetExp = Let("myDict", DictInit([	("Alice",Eint(7)); ("Bob",Eint(3)); ("Charlie",Eint(42))]), secondLetExp);;
	print (eval firstLetExp env0);;
(*Mi aspetto 4 7 5 10 Unbound 107 5040 come output*)


print_string ("*********TEST AMBIENTE DINAMICO********\n");;
	
(*TEST AMBIENTE DINAMICO*)
	(*
		let x = 10 in
			let increase y = y + x in
				let x = 100 in
					increase 5
	*)
	let usageExp = FunCall(Den("increase"), Eint(5));;
	let thirdLetExp = Let("x", Eint(100), usageExp);;
	let secondLetExp = Let("increase", Fun("y", Sum(Den "y", Den("x"))),thirdLetExp)
	let firstLetExp = Let("x", Eint(10), secondLetExp);;
	print (rt_eval firstLetExp);;
	
	let env0 = emptyenv Unbound;;
	let usageExp = FunCall(Den("increase"), Eint(5));;
	let thirdLetExp = Let("x", Eint(100), usageExp);;
	let secondLetExp = Let("increase", Fun("y", Sum(Den "y", Den("x"))),thirdLetExp)
	let firstLetExp = Let("x", Eint(10), secondLetExp);;
	print (eval firstLetExp env0);;
(*mi aspetto 105 e 15*)

print_string("ESECUZIONE COMPLETATA CORRETTAMENTE");;