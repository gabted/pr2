public class DataNotFoundException extends Exception {
    public DataNotFoundException(String user, Object data){
        super("l'utente "+user+" non possiede il dato "+data.toString());
    }
}
