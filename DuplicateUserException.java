public class DuplicateUserException extends Exception {
    public DuplicateUserException(String s){
        super("Nome utente \""+s+"\" non disponibile");
    }
}
