public class IncorrectPasswordException extends Exception {
    public IncorrectPasswordException(String user){
        super("Password errata per l'utete "+user);
    }
}
