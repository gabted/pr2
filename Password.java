public interface Password {
	/*
	OVERVIEW:   gli oggetti di tipo password sono in grado di conservare una stringa di testo,
				in forma crittata o meno, e di verificare se una data stringa in input è uguale alla stringa salvata
	TYPICAL ELEMENT: <psw>
	 */


	/*
	REQUIRES: s != null
	EFFECTS: restituisce true sse s corrisponde a psw
	THROWS: NullPointerException se s == null
	 */

	public boolean match(String s);
}
