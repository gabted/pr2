import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public class SHA2Password implements Password {
	private byte[] psw;

	public SHA2Password(String s){

		if(s == null) {
			throw new NullPointerException();
		}
		else{
			try {
				//inizializzo un digest tramite SHA2 e salvo dentro psw l'hash SHA2 di s
				// digest prende in input e in output dei array di byte
				MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
				psw = messageDigest.digest(s.getBytes());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public boolean match(String s) {

		if(s == null) {
			throw new NullPointerException();
		}
		else{
			try {
				//inizializzo un digest tramite SHA2 e confronto l'hash SHA2 di s
				//con psw, salvato già in forma crittata
				MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
				byte[] hashedInput = messageDigest.digest(s.getBytes());
				return Arrays.equals(psw, hashedInput);

			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return false;
			}
		}
	}
}
