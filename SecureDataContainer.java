import java.util.Iterator;

public interface SecureDataContainer<E> {
    /*  OVERVIEW: Contenitore di oggetti di tipo E a cui è possibile accedere e modificare i dati, solo
                    se ne si è autorizzati.
        TYPICAL ELEMENT: {<id0, psw0, P0, S0>, .., <idn, pswn, Pn, Sn>}
                          dove Pi = insieme di elementi di tipo E che appartengono all'utente i in modo esclusivo
                               Si = insieme di coppie <E, {u1,,uk]> che contengono un elemento E e una lista di
                                    utenti che hanno tutti diritto di accedere o rimuovere l'elemento E,
                                    compreso l'utente i
     */


    /*  REQUIRES: id != null && passw != null && id non già presente in this
        MODIFIES: this
        EFFECTS: aggiunge alla collezione un Utente di nome id e password passw
        THROWS: NullPointerException se id == null || passwd == null
                DuplicateUserException se esiste già un utente di nome id
     */
    public void createUser (String id, String passw) throws DuplicateUserException;



    /*  REQUIRES: owner != null && passw != null &&
                  owner è presente in this && ad owner è associata la psw passw
        MODIFIES: null
        EFFECT: restituisce il numero di elementi dell'utente owner, sia proprietari che condivisi
        THROWS: NullPointerException se owner == null || passwd == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
     */
    public int getSize(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException;


    /*  REQUIRES: owner != null && passw != null && data != null &&
                  owner è presente in this && ad owner è associata la psw passw
        MODIFIES: this
        EFFECT: Aggiunge l'elemento data di proprietà dell'utente owner
                restituisce sempre true
        THROWS: NullPointerException se owner == null || passwd == null || data == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
     */
    public boolean put(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException;



    /*  REQUIRES: owner != null && passw != null && data != null &&
                  owner è presente in this && ad owner è associata la psw passw &&
                  this contiene un elemento data di proprietà di owner
        MODIFIES: null
        EFFECT: restutuisce un riferimento al'oggetto data di proprietà esclusiva di user
                se non esiste tale ogetto, restituisce un riferimento al primo ogetto data condiviso fra owner e altri utenti
        THROWS: NullPointerException sse owner == null || passwd == null || data == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
                DataNotFoundException se non esiste un dato data appartente a owner
     */
    public E get(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException;



    /*  REQUIRES: owner != null && passw != null && data != null &6
                  owner è presente in this && ad owner è associata la psw passw
        MODIFIES: this
        EFFECT: rimuove tutte le occorrenze di data di proprietà dell'utente owner
                restituisce data, se presente, altrimenti restituisce null
        THROWS: NullPointerException se owner == null || passwd == null || data == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
     */
    public E remove(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException;



    /*  REQUIRES: owner != null && passw != null && data != null &&
                  owner è presente in this && ad owner è associata la psw passw &&
                  this contiene un elemento data di proprietà di owner
        MODIFIES: this
        EFFECT: crea un altra copia di data, di proprietà esclusiva di owner, all'interno di this
        THROWS: NullPointerException se owner == null || passwd == null || data == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
                DataNotFoundException se non esiste un dato data appartente a owner
     */
    public void copy(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException;



    /*  REQUIRES: owner != null && passw != null && other != null && data != null &&
                  owner è presente in this && ad owner è associata la psw passw &&
                  this contiene un elemento data di proprietà di owner &&
                  other è presente in this
        MODIFIES: this
        EFFECT: condivide l'ogetto data con l'utente other, in modo che other possa accedervi e/o rimuoverlo
        THROWS: NullPointerException se owner == null || passwd == null || other == null || data == null
                UserNotFoundException se owner non appartiene a this || other non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
                DataNotFoundException se non esiste un dato data appartente a owner
     */
    public void share(String owner, String passw, String other, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException;



    /*  REQUIRES: owner != null && passw != null &&
                  owner è presente in this && ad owner è associata la psw passw
        MODIFIES: null
        EFFECT: restituisce un Iterator su tutti i data appartententi a owner, sia esclusivi che condivisi
        THROWS: NullPointerException se owner == null || passwd == null
                UserNotFoundException se owner non appartiene a this
                IncorrectPasswordException se la password non coincide con quella di owner
     */
    public Iterator<E> getIterator(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException;

    /*
        REQUIRES: s != null
        EFFECTS: stampa a schermo una rappresentazione esaustiva dell'utente s
        THROWS: NullPointerException se s == null
     */
    public void printUser(String s);

}
