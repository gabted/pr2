import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SecureDataContainer1<E> implements SecureDataContainer<E> {
	/*

	Alpha_SDC1(c) = {Alpha_User1(c.users.get(i)) | 0 <= i < c.users.size()-1}

	Inv_SDC1(c) = c.users != null &&
				  ForAll i / (0 <= i < c.users.size()-1) ==> c.users.get(i) != null &&
				  ForAll i / (0 <= i < c.users.size()-1) ==> Inv_User1(c.users.get(i)) &&
				  ForAll i,j / ((i,j = 0 ,, c.users.size()-1)&&(i!=j)) ==> !c.users.get(i).id.equals(c.users.get(j).id)                         //utenti tutti distinti
				  ForAll i (0 <= i < c.users.size()-1) ==> ForAll j / (0 <= j <  c.users.get(i).shared.size()-1) ==> ForAll k / (0 <= k < c.users.get(i).shared.get(j)owners.size()-1) ==>
				        Exists s / (0 <= s < c.users.size()-1) && c.users.get(s).id.equals(c.users.get(i).shared.get(j).owners.get(k)) &&       //i nomi contenuti in Owner di ogni shared data sono effettivamente nomi di utenti registrati
				  ForAll i (0 <= i < c.users.size()-1) ==> ForAll j / (0 <= j < c.users.get(i).shared.size()-1) ==> ForAll k / (0 <= k < c.users.get(i).shared.get(j).owners.size()-1) ==>
				        Exists s / (0 <= s < c.users.size()-1) && Exists t / (0 <= t < c.users.get(i).shared.size()-1) && Exists u / (0 <= u < c.users.get(i).shared.owners.size()-1) &&
				            c.users.get(i).shared.get(j).value == c.users.get(s).shared.get(t).value &&                                         // | quando viene condiviso, un dato deve essere presente nelle liste
				            c.users.get(i).shared.get(j).owners.get(k).equals(c.users.get(s).shared.get(t).owners.get(u))                       // | shared di entrambi gli utenti

	 */


	 
	 
	 
    private List<User1<E>> users;

    public SecureDataContainer1(){
    	users = new ArrayList<User1<E>>();
    }

    //EFFECTS: restituisce l'utente con nome uguale a s, altrimenti null
	private User1<E> select(String s){
		User1<E> out = null;
		Iterator<User1<E>> itr = this.users.iterator();
		while(itr.hasNext() && out == null){
			User1 curr = itr.next();
			if(curr.confrontId(s)){
				out = curr;
			}
		}
		return out;
	}

	//EFFECTS: restituisce true sse è già presente un utente di nome s in this
    private boolean contains(String u){
        Iterator<User1<E>> itr = users.iterator();
        boolean found = false;
	    while(itr.hasNext() && !found){
		    found = itr.next().confrontId(u);
	    }
        return found;
    }

    @Override
	public void printUser(String s){
		select(s).print();
	}

    @Override
    public void createUser(String id, String passw) throws DuplicateUserException {
    	if(id == null || passw == null) {
		    throw new NullPointerException();
	    }
    	else{
		    if (this.contains(id))
			    throw new DuplicateUserException(id);
		    else
			   users.add(new User1<E>(id, passw));
    	}
    }


    @Override
    public int getSize(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException {
	    if(owner == null || passw == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1 selected = select(owner);
		    if(selected == null)                                    //select(String s) restituisce null se non esiste utente di nome s
			    throw new UserNotFoundException(owner);
		    else if(!selected.confrontPsw(passw))                   //confrontPsw(p) restituisce true sse la password corrisponde
		    	throw new IncorrectPasswordException(owner);
		    else{
		    	return selected.getSize();
		    }
	    }
    }

    @Override
    public boolean put(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException {
	    if(owner == null || passw == null || data == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1<E> selected = select(owner);
		    if(selected == null)
			    throw new UserNotFoundException(owner);
		    else if(!selected.confrontPsw(passw))
			    throw new IncorrectPasswordException(owner);
		    else{
			    selected.addPersonalData(data);
			    return true;
		    }
	    }
    }

	@Override
	public void copy(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException {
		if(owner == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User1<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else if(!selected.containsPersonalData(data))
				throw new DataNotFoundException(owner, data);
			else{
				selected.addPersonalData(data);
			}
		}
	}

    @Override
    public E get(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException {
	    if(owner == null || passw == null || data == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1<E> selected = select(owner);
		    if(selected == null)
			    throw new UserNotFoundException(owner);
		    else if(!selected.confrontPsw(passw))
			    throw new IncorrectPasswordException(owner);
		    else {          //se utente e psw sono corretti, prima cerca in personal, poi in shared, se non trova lancia un eccezione
			    if (selected.containsPersonalData(data))
				    return selected.getPersonalData(data);
			    else if(selected.containsSharedData(data))
				    return selected.getSharedData(data).getData();
			    else
				    throw new DataNotFoundException(owner, data);
		    }
	    }
    }

    @Override
    public E remove(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException{
	    if(owner == null || passw == null || data == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1<E> selected = select(owner);
		    if(selected == null)
			    throw new UserNotFoundException(owner);
		    else if(!selected.confrontPsw(passw))
			    throw new IncorrectPasswordException(owner);
		    else {          //se utente e psw sono corretti

				//se trovo data in personal o in shared imposto found a true
			    //se non lo trovo in nessuno dei due found rimane false e restituisco null
		    	boolean found = false;
		    	//rimuovo prima da personal
			    if (selected.containsPersonalData(data)){
				    found = true;
				    selected.removePersonalData(data);          //removeFromPersonal rimuove tutte le occorrenze di data, non solo la prima
			    }
			    //poi rimuovo da shared
			    if(selected.containsSharedData(data)) {
				    found = true;
				    /*
				        Elimino ogni occorrenza di data da selected, ma prima devo eliminare
				        ciascuna occorrenza di data da tutti i suoi altri proprietari, e solo alla fine posso eliminare ciascuna
				        occorrenza da selected.
				        In pseudocodice:
				        for(element con valore data in selected.shared)
				            for(owner in element.owners)
				                owner.remove(element), se element è una delle occorrenze di data appartenenti a selected
				            selected.remove(element)
				     */
				    Iterator<SharedData<E>> SDIterator = selected.getSharedIterator();
				    while(SDIterator.hasNext()){
					    SharedData currentData =  SDIterator.next();
					    if(currentData.confrontData(data)) {
					        Iterator<String> owners = currentData.getOwnersIterator();      //per ogni SD di owner con valore data
					        while (owners.hasNext()) {
						        String currentOwner = owners.next();
						        if (currentOwner != owner) {
							        select(currentOwner).removeSharedData(data, owner);      //rimuovo le sue occerrenze da tutti gli altri utenti
						        }
					        }
					        SDIterator.remove();
				        }
				    }
			    }

			    //infine restituisco il valore adatto
			    if(found)
			    	return data;
			    else
			    	return null;

		    }
	    }
    }

    @Override
    public void share(String owner, String passw, String other, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException{
	    if(owner == null ||other == null || passw == null || data == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1<E> sender = select(owner);
		    User1<E> reciever = select(other);
		    if(sender == null )
			    throw new UserNotFoundException(owner);
		    if(reciever == null )
			    throw new UserNotFoundException(other);
		    else if(!sender.confrontPsw(passw))
			    throw new IncorrectPasswordException(owner);
		    else {

		    	/*
		    	    defininendo ContieneSD(x) = c.shared.contains(data), possiamo trovare solo 4 casi possibili quando
		    	    sender condivide il dato data con reciever:
		    	    CASO A ContieneSD(sender) && ContieneSD(reciever)
		    	        SKIP
		    	    CASO B ContieneSD(sender) && !ContieneSD(reciever)
		    	        aggiungo other dentro SD in sendere aggiungo SD in reciever
		    	    CASO C !ContieneSD(sender) && ContieneSD(reciever)
						creo un nuovo SD e lo aggiungo in entrambi
		    	    CASO D !ContieneSD(sender) && !ContieneSD(reciever)
		    	        creo un nuovo SD e lo aggiungo in entrambi
		    	 */


			    //sender ha precedentemente condiviso data con qualcuno, quindi data è presente in shared
			    if (sender.containsSharedData(data)) {
				    if (!reciever.containsSharedData(data)) {     //CASO B, devo condividere con other tutti gli elementi data che appartengono a owner
				    	Iterator<SharedData<E>> itr = sender.getSharedIterator();
				    	while(itr.hasNext()){
						    SharedData<E> s_data = itr.next();
						    if(s_data.confrontData(data)) {
							    s_data.addOwner(other);
							    reciever.addSharedData(s_data);
						    }
				    	}
				    }
			    }
			    //sender non ha mai condiviso data, quindi è ancora nella sua lista personal ma non in quella shared
			    else if(sender.containsPersonalData(data)){     //CASO B e C
			    	SharedData<E> s_data = new SharedData<E>(data);
			    	s_data.addOwner(owner);
			    	s_data.addOwner(other);
			    	sender.addSharedData(s_data);
			    	reciever.addSharedData(s_data);
			    	sender.removePersonalData(data);
			    }

			    //data non è contenuto nè in Shared ne in personal
			    else
			    	throw new DataNotFoundException(owner, data);
		    }
	    }
    }

    @Override
    public Iterator<E> getIterator(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException {
	    if(owner == null || passw == null) {
		    throw new NullPointerException();
	    }
	    else {
		    User1<E> selected = select(owner);
		    if(selected == null)
			    throw new UserNotFoundException(owner);
		    else if(!selected.confrontPsw(passw))
			    throw new IncorrectPasswordException(owner);
		    else
		    	return selected.getMergedIterator();    //MergedIterator crea un nuovo Iterator concatenando
		                                                // personal.iterator e shared.iterator
	    }
    }
}
