

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class SecureDataContainer2 <E> implements SecureDataContainer <E> {
	/*
	Alpha_SDC2(c) = {<c.users.get(i).id, c.users.get(i).psw, Pi, Si>| i = 0 ,, c.users.size()-1}
																	 Pi = {c.users.get(i).personal.get(j) | j = 0 ,, c.users.get(i).personal.size()-1}
																	 Si = {Alpha_SharedData(c.shared.get(j)) | j = 0 ,, c.shared.size()-1}
																	                                           c.data.get(j).owners.contains(c.users.get(i).id)

	Inv_SDC2(c) = c.users != null && c.shared != null &&
				  ForAll i / (i = 0 ,, c.users.size()-1) ==> c.users.get(i) != null &&
				  ForAll i / (i = 0 ,, c.users.size()-1) ==> Inv_User2(c.users.get(i)) &&
				  ForAll i / (i = 0 ,, c.shared.size()-1) ==> c.shared.get(i) != null &&
				  ForAll i / (i = 0 ,, c.shared.size()-1) ==> Inv_SharedData(c.shared.get(i)) &&
				  ForAll i,j / ((i,j = 0 ,, c.users.size()-1)&&(i!=j)) ==> !c.users.get(i).id.equals(c.users.get(j).id)                     //utenti tutti distinti
				  ForAll i / (i = 0 ,, c.shared.size()-1) ==> ForAll j / (j = 0 ,, c.shared.get(i).owners.size()-1) ==>
				        Exists k / (k = 0 ,, c.users.size()-1) && c.shared.get(i).owners.get(j).equals(c.users.get(k).id)                   //i nomi contenuti in Owner di ogni shared data sono effettivamente nomi di utenti registrati
	 */

	private List<User2<E>> users;
	private List<SharedData<E>> shared;             //lista di dati shared globali, comuni a tutti gli utenti

	public SecureDataContainer2() {
		this.users = new ArrayList<User2<E>>();
		this.shared = new ArrayList<SharedData<E>>();
	}


	//EFFECTS: restituisce true sse è già presente un utente di nome s in this
	private boolean contains(String u){
		Iterator<User2<E>> itr = users.iterator();
		boolean found = false;
		while(itr.hasNext() && !found){
			found = itr.next().confrontId(u);
		}
		return found;
	}

	//EFFECTS: restituisce l'utente con nome uguale a s, altrimenti null
	private User2<E> select(String s) {
		User2<E> out = null;
		Iterator<User2<E>> itr = this.users.iterator();
		while (itr.hasNext() && out == null) {
			User2 curr = itr.next();
			if (curr.confrontId(s)) {
				out = curr;
			}
		}
		return out;
	}

	@Override
	public void printUser(String s){
		if(s == null)
			throw new NullPointerException();
		else {
			System.out.println("Utente " + s + " con elementi:");


			System.out.printf("\tpersonali \t");        //i dati personali sono presi da user.personal
			for (Iterator<E> itr = select(s).getPersonalIterator(); itr.hasNext(); )
				System.out.printf("%s, ", itr.next().toString());
			System.out.printf("\n");

			System.out.printf("\tcondivisi \t");       //i dati condivisi sono cercati all'interno di shared
			for (SharedData<E> e : shared)
				if (e.containsOwner(s))
					System.out.printf("%s, ", e.getData());
			System.out.printf("\n");
		}

	}

	@Override
	public void createUser(String id, String passw) throws DuplicateUserException {
		if(id == null || passw == null) {
			throw new NullPointerException();
		}
		else{
			if (this.contains(id))
				throw new DuplicateUserException(id);
			else
				users.add(new User2<E>(id, passw));
		}
	}

	@Override
	public int getSize(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException {
		if(owner == null || passw == null) {
			throw new NullPointerException();
		}
		else {
			User2 selected = select(owner);
			if(selected == null)                                    //select(String s) restituisce null se non esiste utente di nome s
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))                   //user.confrontPsw(p) restituisce true sse la password corrisponde
				throw new IncorrectPasswordException(owner);
			else{
				//conto gli elementi in shared appartenenti a owner e poi vi sommo gli elementi proprietari di owner
				int counter = 0;
				for (SharedData<E> element : this.shared) {
					if(element.containsOwner(owner))
						counter++;
				}
				return counter+selected.getSize();
			}
		}
	}

	@Override
	public boolean put(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException {
		if(owner == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else{
				selected.addPersonalData(data);
				return true;
			}
		}
	}

	@Override
	public void copy(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException {
		if(owner == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else if(!selected.containsPersonalData(data))
				throw new DataNotFoundException(owner, data);
			else{
				selected.addPersonalData(data);
			}
		}
	}

	@Override
	public E get(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException {
		if(owner == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else {          //se utente e psw sono corretti, prima cerca in personal, poi in shared, se non trova lancia un eccezione
				if (selected.containsPersonalData(data))
					return selected.getPersonalData(data);
				else {
					Iterator<SharedData<E>> itr = shared.iterator();
					SharedData<E> out = null;
					while(itr.hasNext() && out==null){
						SharedData<E> curr = itr.next();
						if(curr.confrontData(data)&&curr.containsOwner(owner))
							out = curr;
					}
					if (out != null)
						return out.getData();
					else
						throw new DataNotFoundException(owner, data);
				}
			}
		}
	}

	@Override
	public E remove(String owner, String passw, E data) throws UserNotFoundException, IncorrectPasswordException {
		if(owner == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else {          //se utente e psw sono corretti
				//se trovo data in selected.personal o in this.shared imposto found a true
				//se non lo trovo in nessuno dei due found rimane false e restituisco null
				boolean found = false;

				//rimuovo prima da personal
				if (selected.containsPersonalData(data)){
					selected.removePersonalData(data);          //removeFromPersonal rimuove tutte le occorrenze di data, non solo la prima
					found = true;
				}

				//poi rimuovo da shared
				for(Iterator<SharedData<E>> itr = shared.iterator(); itr.hasNext();){
					SharedData<E> curr = itr.next();
					if(curr.confrontData(data) && curr.containsOwner(owner)){
						itr.remove();
						found = true;
					}
				}

				//infine restituisco il valore adatto
				if(found)
					return data;
				else
					return null;
			}
		}
	}


	@Override
	public void share(String owner, String passw, String other, E data) throws UserNotFoundException, IncorrectPasswordException, DataNotFoundException {
		if(owner == null ||other == null || passw == null || data == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> sender = select(owner);
			User2<E> reciever = select(other);
			if(sender == null )
				throw new UserNotFoundException(owner);
			if(reciever == null )
				throw new UserNotFoundException(other);
			else if(!sender.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else {

				//prima controllo se owner ha già condiviso il dato con qualcuno, cioè se è già presente un dato
				//data di proprietà di owner dentro shared
				//ogni dato che trovo, se non appartiene già anche ad other, vi aggiungo other come proprietario
				boolean found  = false;
				for (SharedData<E> el : shared) {
					if(el.confrontData(data)&&el.containsOwner(owner)&&!el.containsOwner(other)){
						el.addOwner(other);
						found = true;
					}
				}

				//se data non era in shared, o lo trovo in personal, o non è un dato appartentente a owner, quindi lancio una eccezione
				if(!found) {
					//data è in personal, creo un nuovo elemento sharedData e rimuovo data da personal
					if (sender.containsPersonalData(data)) {
						SharedData<E> s_data = new SharedData<E>(data);
						s_data.addOwner(owner);
						s_data.addOwner(other);
						this.shared.add(s_data);
						sender.removePersonalData(data);
					}

					//data non è contenuto nè in Shared ne in personal
					else
						throw new DataNotFoundException(owner, data);
				}
			}
		}
	}

	@Override
	public Iterator<E> getIterator(String owner, String passw) throws UserNotFoundException, IncorrectPasswordException {
		if(owner == null || passw == null) {
			throw new NullPointerException();
		}
		else {
			User2<E> selected = select(owner);
			if(selected == null)
				throw new UserNotFoundException(owner);
			else if(!selected.confrontPsw(passw))
				throw new IncorrectPasswordException(owner);
			else{
				return new FilteredIterator(owner);     //uso una classe interna non anonima, poichè ho bisogno di inizializzare
														//FilteredIterator prima di usarlo per la prima volta, cioè ho bisogno di un costruttore
			}
		}
	}

	private class FilteredIterator implements Iterator<E> {
		/*OVERVIEW: è un Iterator sulla lista shared della classe esterna, filtrando però questa lista
					affinchè i metodi next e hasNext cerchino e restituiscano solo gli elementi in shared
					che sono di proprietà di owner. Formalmente, è un iteratore sull'insieme
					{e.getData | shared.contains(e) && e.containsOwner(owner)}
		*/
		//uso come proprietà i 2 iteratori distinti, il nome dell'utente per il quale voglio filtrare,
		Iterator<E> personalItr;
		Iterator<SharedData<E>> sharedItr;
		String owner;
		//e due valori, quelli che verranno restituiti dai metodi dell'interfaccia una volta esaurito il primo itr.
		//questi due valori vengono aggiornati da searchNextElement, che sposta sharedCurrentElementi in avanti
		//fino al prossimo elemento all'interno di sharedItr di cui owner è proprietario.
		boolean sharedHasNext;
		SharedData<E> sharedCurrentElement;

		public FilteredIterator(String s){
			this.owner = s;
			this.personalItr = select(s).getPersonalIterator();
			this.sharedItr = shared.iterator();
			searchNextEl();
		}

		private void searchNextEl(){
			boolean found = false;
			while(sharedItr.hasNext()&&!found){
				sharedCurrentElement = sharedItr.next();
				found = sharedCurrentElement.containsOwner(owner);
			}
			sharedHasNext = found;

		}

		@Override
		public boolean hasNext() {
			return personalItr.hasNext() || this.sharedHasNext;
		}

		@Override
		public E next() {
						/*
							se next venisse chiamato quando this.hasNext restituisce false:
								this.sharedCurrentElement sarebbe impostato all'ultimo elemento di
									shared.Itr, che è un valore non significativo, per via di come è
									implementato searchNextEl();
								this.sharedHasNext invece sarebbe impostato a false, e quindi verrebbe
									eseguito l'ultimo ramo else, che lancia una eccezione. Così facendo
									questa implementazione di iterator è un sottotipo legale di Iterator,
									poichè verifica la regola dei metodi del principio di sostituzione
						*/

			if (personalItr.hasNext())
				return personalItr.next();
			else{
				if(!this.sharedHasNext)
					throw new NoSuchElementException();
				else {
					E old = this.sharedCurrentElement.getData();
					searchNextEl();
					return old;
					//così facendo, restituisco il vecchio valore di sharedCurrentElement, ma eseguo searchNextEl() affinchè
					//le prossime chiamate di hasNext() e next() abbiano già i valori aggiornati
				}
			}
		}
	}

}
