import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SharedData <E>{
	/*
	OVERVIEW: è un wrapper per il dato E, a cui accostiamo una lista di stringhe che rappresentano gli utenti
			  che possono accedervi, modificarlo e rimuoverlo. Viene utilizzato per entrambe le implementazioni.
	TYPICAL ELEMENT: <E, {u1, u2, .., uk}>

	Alpha_SharedData(c) = <c.value, {owners.get(i) | 0 <= i <c.owners.size()-1}

	Inv_SharedData(c) = c.value != null && c.owners != null && c.owners.size() >= 2 &&
				ForAll i / (0 <= i <c.owners.size()-1) == > c.owners.get(i) != null
				ForAll i,j / (0 <= i < j <c.owners.size()-1) == > c.owners.get(i) != c.owners.get(j)
	 */

	private E value;
	private List<String> owners;

	public SharedData(E data) {
		value = data;
		owners = new ArrayList<String>();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SharedData){
			return this.getData().equals(((SharedData<E>) obj).getData());
		}
		else
			return super.equals(obj);
	}

	public E getData(){
		return value;
	}

	public boolean confrontData(E data){
		return this.value.equals(data);
	}

	public void addOwner(String s){
		this.owners.add(s);
	}

	public Iterator<String> getOwnersIterator(){
		return owners.iterator();
	}

	public boolean containsOwner(String s){
		return this.owners.contains(s);
	}
}
