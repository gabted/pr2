import java.util.Iterator;

public class Test {
	/*
		README:
		programma implementato e compilato in Java 8
		Questa classe utilizza gli utenti testUsers per testare entrambe le implementazioni di SecureDataContainer
		E' possibile scegliere quale implementazione testare cambiando il valore della proprietà booleana
		"implementazione1", quando è vera verà testata SecureDataContainer1, quando è falsa SecureDataContainer2
		VANNO ATTIVATE LE ASSERTION, poichè vengono usate dopo ogni comando per verificare di aver ottenuto
		il risultato previsto.
	 */

	static boolean implementazione1 = false;
	static String[] testUsers = {"Alice", "Bob", "Charlie", "David", "Eve"};
	static String[] testPassw = {"aaa", "bbb", "ccc", "ddd", "eee"};
	static int n = 5;

	public static void main(String[] args) {
		SecureDataContainer<String> container = (implementazione1)?(new SecureDataContainer1<String>()):(new SecureDataContainer2<String>());


		System.out.println("Inizio Test SecureDataContainer su implementazione "+((implementazione1)?1:2));
		if(implementazione1)
			assert container instanceof SecureDataContainer1;
		else
			assert container instanceof SecureDataContainer2;

		/*
			TEST inserimento degli utenti:
		 */
		for (int i = 0; i < n; i++) {
			try {
				container.createUser(testUsers[i], testPassw[i]);
				System.out.printf("Inserito %s\n", testUsers[i]);
			}
			catch (DuplicateUserException e){
				e.printStackTrace();    //non viene mai sollevata
			}
		}
		System.out.println();

		try {

			//contatori per le eccezioni volutamente sollevate, utile per verificare il funzionamento dei metodi e delle eccezioni checked
			int exceptionsThrowed = 0;


			/*
				TEST METODO PUT
			 */
			container.put("Alice", "aaa", "Java");
			container.put("Alice", "aaa", "C#");
			container.put("Alice", "aaa", "C++");
			assert container.getSize("Alice", "aaa") == 3;
			assert container.get("Alice", "aaa", "Java").equals("Java");
			assert container.get("Alice", "aaa", "C#").equals("C#");
			assert container.get("Alice", "aaa", "C++").equals("C++");
			try{
				container.get("Alice", "aaa", "PHP");
			}
			catch(DataNotFoundException e){
				exceptionsThrowed++;        //eccezione previsa, Alice non contiene "PHP"
			}
			assert exceptionsThrowed == 1;


			/*
				TEST METODO REMOVE
			 */
			container.put("Bob", "bbb", "LISP");
			container.put("Bob", "bbb", "ML");
			container.put("Bob", "bbb", "CaML");
			assert container.getSize("Bob", "bbb") == 3;

			container.remove("Bob", "bbb", "LISP");
			assert container.getSize("Bob", "bbb") == 2;
			container.remove("Bob", "bbb", "Algoritmica");
			assert container.getSize("Bob", "bbb") == 2;
			try{
				container.get("Bob", "bbb", "LISP");
			}
			catch(DataNotFoundException e){
				exceptionsThrowed++;
			}
			assert exceptionsThrowed == 2;


			/*
				TEST METODO COPY
			 */
			container.copy("Alice", "aaa", "Java");
			container.copy("Alice", "aaa", "Java");
			container.copy("Alice", "aaa", "Java");
			assert container.getSize("Alice", "aaa") == 6;

			container.remove("Alice", "aaa", "Java");       //remove rimuove tutte le occorrenze di "Java"
			assert container.getSize("Alice", "aaa") == 2;
			try{
				container.get("Alice", "aaa", "Java");
			}
			catch(DataNotFoundException e){
				exceptionsThrowed++;
			}
			assert exceptionsThrowed == 3;


			/*
				TEST METODO GETITERATOR
			 */
			container.put("Alice", "aaa", "Pascal");
			container.put("Alice", "aaa", "Visual Basic");
			container.put("Alice", "aaa", "Algol");
			//Alice ha aggiunto come dati Java, C#, C++, Pascal, Visual Basic, Algol
			//e nel frattempo ha rimosso Java
			//mi aspetto quindi che Alice contenga solo i seguenti:
			String[] expected = {"C#", "C++", "Pascal", "Visual Basic", "Algol"};
			Iterator<String> itr;
			itr = container.getIterator("Alice", "aaa");
			for (int i = 0; i < container.getSize("Alice", "aaa"); i++) {
				assert itr.next().equals(expected[i]);
			}


			/*
				TEST METODO SHARE
			 */
			container.share("Alice", "aaa", "Charlie", "C#");
			assert container.getSize("Charlie", "ccc") == 1;
			assert container.get("Charlie", "ccc", "C#") == "C#";
			assert container.getSize("Alice", "aaa") == 5;                //la size di Alice non cambia, semplicemente perde l'esclusività
																						//dei permessi di operare sul dato "C#"

			container.share("Alice", "aaa", "David", "C#");   //"C#", già condiviso fra Alice e Charlie, viene condiviso anche con David
			assert container.getSize("David", "ddd") == 1;
			assert container.get("David", "ddd", "C#") == "C#";


			/*
				TEST METODO REMOVE DI ELEMENTI SHARED
			 */
			container.remove("David", "ddd", "C#");                 //David rimuove "C#", rimuovendolo anche per tutti gli utenti con cui era condiviso
			assert container.getSize("David", "ddd") == 0;
			assert container.getSize("Charlie", "ccc") == 0;
			assert container.getSize("Alice", "aaa") == 4;
			try{
				container.get("Alice", "aaa", "C#");
			}
			catch(DataNotFoundException e){
				exceptionsThrowed++;
			}
			assert exceptionsThrowed == 4;


			/*
				TEST METODO SHARED DI ISTANZE DIVERSE
			 */
			container.put("Alice", "aaa", "JavaScript");
			container.share("Alice", "aaa", "Eve","JavaScript");
			
			container.put("Charlie", "ccc", "JavaScript");
			container.share("Charlie", "ccc", "Eve","JavaScript");
			assert container.getSize("Eve", "eee") == 2;                  //Alice è destinataria di due ogetti diversi di valore "JavaScript",
																					    //una condivisa fra Eve e Alice, l'altra condivisa fra Eve e Charlie
																						//Visto che Alice non ha mai condiviso "JavaSscript" con Charlie, è giusto che
																						//i 2 ogetti rimangano separati

			container.share("Eve", "eee", "David","JavaScript");
			assert container.getSize("David", "ddd") == 2;                //entrambi i dati di cui sopra vengono condivisi con David:
																						//uno è condiviso fra Alice, Eve e David,
																						//l'altro fra Charlie, Eve e David


			/*
				TEST METODO REMOVE DI ISTANZE DIVERSE
			 */
			container.remove("Charlie", "ccc", "JavaScript");       //viene eliminato il dato condiviso fra Charlie, Eve e David,
			assert container.getSize("Eve", "eee") == 1;                  //ma rimane quello di Alice, Eve e David
			assert container.getSize("David", "ddd") == 1;


			/*
				TEST FINALE DI CORRETTEZZA
			 */
			String[] expected2 = {"C++", "Pascal", "Visual Basic", "Algol", "JavaScript"};
			Iterator<String> itr2;
			itr2 = container.getIterator("Alice", "aaa");
			for (int i = 0; i < container.getSize("Alice", "aaa"); i++) {
				assert itr2.next().equals(expected2[i]);
			}
			
		}//end try
		 catch (DataNotFoundException e) {
			e.printStackTrace();
		} catch (UserNotFoundException e) {
			e.printStackTrace();
		} catch (IncorrectPasswordException e) {
			e.printStackTrace();
		}
		finally {

			System.out.println("###################");
			for (int i = 0; i < n; i++) {
				container.printUser(testUsers[i]);
			}
			System.out.println("###################");

			System.out.println("Test Superato!");
		}

	}
}