public class UnencodedPassword implements Password {
	private String psw;

	public UnencodedPassword(String s){
		if(s == null) {
			throw new NullPointerException();
		}
		else{
			psw = s;
		}
	}

	@Override
	public boolean match(String s) {
		if(s == null) {
			throw new NullPointerException();
		}
		else{
			return this.psw.equals(s);
		}
	}
}
