import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class User1<E> {
	/*
	OVERVIEW: è un tipo di dato che contiene identificativo, password e 2 liste di dati, una di dati esclusi, di tipo E,
			  l'altra di dati condivisi con altri utenti, di tipo SharedData<E>
			  Utilizza inoltre un parametro di tipo Password per conservare e verificare la password
	TYPICAL ELEMENT: <id, psw, P, S>
					dove P = {e1, e2, .., en}
						 S = {<e1,{u11, u12,, u1k}>, .., <em,{um1, um2,, umk}>}

	Alpha_User1(c) = <c.id, c.psw, {c.personal.get(i) | 0 <= i < c.shared.size()-1}, {Alpha_SharedData(c.shared.get(i)) | 0 <= i < c.shared.size()-1}>

	Inv_User1(c) = c.id != null && c.psw != null && c.personal != null && c.shared != null &&
				   ForAll i / (0 <= i < c.personal.size()-1) == > c.personal.get(i) != null &&
				   ForAll i / (0 <= i < c.shared.size()-1) == > c.shared.get(i) != null &&
				   ForAll i / (0 <= i < c.shared.size()-1) == > Inv_SharedData(c.shared.get(i))
				   ForAll i / (0 <= i < c.shared.size()-1) == > Exists j / 0 <= j < c.shared.get(i).owners.size()-1 && c.shared.get(i).owners.get(j).equals(c.id)
	 */


	private String id;
	private Password psw;
	private List<E> personal;
	private List<SharedData<E>> shared;

	public User1(String id, String psw) {
		this.id = id;
		this.psw = new SHA2Password(psw);
		this.personal = new ArrayList<E>();
		this.shared = new ArrayList<SharedData<E>>();
	}

	public boolean confrontPsw(String s){
		return psw.match(s);
	}


	public boolean confrontId(String s) {
		return id.equals(s);
	}

	public int getSize(){
		return this.personal.size() + this.shared.size();
	}

	public void print(){
		System.out.println("Utente "+id+" con elementi:");

		System.out.printf("personali \t");
		for (E e : personal)
			System.out.printf("%s, ", e);
		System.out.printf("\n");

		System.out.printf("condivisi \t");
		for (SharedData<E> e : shared)
			System.out.printf("%s, ", e.getData());
		System.out.printf("\n");

	}



	public boolean containsPersonalData(E data){
		return this.personal.contains(data);
	}

	public void addPersonalData(E data){
		this.personal.add(data);
	}

	public E getPersonalData(E data){
		return personal.get(personal.indexOf(data));
	}

	public E removePersonalData(E data){
		while(this.personal.contains(data))
			this.personal.remove(data);
		return data;
	}




	public boolean containsSharedData(E data){
		Iterator<SharedData<E>> itr = shared.iterator();
		boolean found = false;
		while(itr.hasNext() && !found){
			found = itr.next().confrontData(data);
		}
		return found;
	}

	public void addSharedData(SharedData<E> s_data){
		this.shared.add(s_data);
	}

	public SharedData<E> getSharedData(E data){
		SharedData<E> out = null;
		Iterator<SharedData<E>> itr = this.shared.iterator();
		while(itr.hasNext() && out == null){
			SharedData curr = itr.next();
			if(curr.confrontData(data)){
				out = curr;
			}
		}
		return out;
	}

	//rimuove tutti gli sharedData di valore E che appartengono all'utente Owner, che può essere diverso da this.id
	public void removeSharedData(E data, String owner){
		Iterator<SharedData<E>> itr = this.shared.iterator();
		while(itr.hasNext()){
		    SharedData curr = itr.next();
		    if(curr.confrontData(data) && curr.containsOwner(owner))
		    	itr.remove();
		}
	}

	public Iterator<SharedData<E>> getSharedIterator(){
		return this.shared.iterator();
	}

	public Iterator<E> getMergedIterator(){
		return new Iterator<E>() {
			//dichiaro una classe interna anonima, che ha 2 proprietà corrispondenti ai 2 iteratori personal e shared
			Iterator<E> personalItr = personal.iterator();
			Iterator<SharedData<E>> sharedItr = shared.iterator();

			@Override
			//controlla prima se ci sono elementi in personalItr, poi in sharedItr
			public boolean hasNext() {
				return personalItr.hasNext() || sharedItr.hasNext();
			}

			@Override
			//restituisce prima gli elementi in personalItr, se non ve ne sono restituisce quelli in sharedItr
			public E next() {
				if(personalItr.hasNext())       //stiamo ciclando personal
					return personalItr.next();

				//se next venisse chiamato quando this.hasNext restituisce false, cioè quando anche sharedItr ha concluso,
				//si entrerebbe nell'else sottostante e sharedItr.next solleverebbe
				//una eccezione NoSuchElementException. Così facendo questa implementazione
				//di iterator rispetta la specifica di java.util.Iterator
				else
					return sharedItr.next().getData();
			}
		};
	}
}
