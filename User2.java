import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class User2<E> {
	/*
	OVERVIEW: è un tipo di dato che contiene identificativo, password e 1 lista di dati esclusi, di tipo E.
			  Utilizza inoltre un parametro di tipo Password per conservare e verificare la password
	TYPICAL ELEMENT: <id, psw, P>
					dove P = {e1, e2, .., en}

	Alpha_User2(c) = <c.id, c.psw, {c.personal.get(i) | i = 0 ,, c.personal.size()-1}>

	Inv_User2(c) = c.id != null && c.psw != null && c.personal != null &&
				   ForAll i / (i = 0 ,, c.personal.size()-1) == > c.personal.get(i) != null
	 */

	private String id;
	private Password psw;
	private List<E> personal;

	public User2(String id, String psw) {
		this.id = id;
		this.psw = new SHA2Password(psw);
		this.personal = new ArrayList<E>();
	}

	public boolean confrontPsw(String s){
		return psw.match(s);
	}

	public boolean confrontId(String s) {
		return id.equals(s);
	}

	public int getSize(){
		return this.personal.size();
	}

	public void print(){
		System.out.println("");
		System.out.println("Utente "+id+" con elementi:");
		for (E e : personal)
			System.out.printf("%s, ", e);
		System.out.printf("\n");
	}

	public boolean containsPersonalData(E data){
		return this.personal.contains(data);
	}

	public void addPersonalData(E data){
		this.personal.add(data);
	}

	public E getPersonalData(E data){
		return personal.get(personal.indexOf(data));
	}

	public E removePersonalData(E data){
		while(this.personal.contains(data))
			this.personal.remove(data);
		return data;
	}

	public Iterator<E> getPersonalIterator(){
		return this.personal.iterator();
	}
}
