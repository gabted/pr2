public class UserNotFoundException extends Exception {
    public UserNotFoundException(String user){
        super("Utente \""+user+"\" non presente");
    }
}
